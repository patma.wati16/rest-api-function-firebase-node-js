const functions = require('firebase-functions');
const express = require ('express');
const cors = require('cors');
const admin = require ('firebase-admin');
const bodyParser =require('body-parser');

const app=express();
app.use(cors({origin:true}));

var serviceAccount=require("./serviceAccountKey.json");
admin.initializeApp({
    credential:admin.credential.cert(serviceAccount),
    databaseURL :"https://function-api-8e5cd.firebaseio.com"
})
const db = admin.firestore();

//menampilkan data cso
app.get('/cso/read', (req, res)=>{
    (async()=>{
        try{
            let query = db.collection('customerservice');
            let response =[];
            await query.get().then(querySnapshot => {
                let docs = querySnapshot.docs;
                for (let doc of docs){
                    const selectedItem={
                        id : doc.id,
                        nama_lengkap : doc.data().nama_lengkap,
                        nama_panggil : doc.data().nama_panggil,
                        email : doc.data().email
                    };
                    response.push(selectedItem);
                }
            });
            return res.status(200).send(response);
        } catch(error){
            console.log(error);
            return res.status(500).send(error);
        }
    })();
})

//menampilkan data antrian
app.get('/antrian/read', (req, res)=>{
    (async()=>{
        try{
            let query = db.collection('antrian');
            let response =[];
            await query.get().then(querySnapshot => {
                let docs = querySnapshot.docs;
                for (let doc of docs){
                    const selectedItem={
                        id :doc.id,
                        no_urut : doc.data().no_urut,
                        waktu_ticket : doc.data().time_ticket.toDate(),
                        status : doc.data().status,
                    };
                    response.push(selectedItem);
                }
            });
            return res.status(200).send(response);
        } catch(error){
            console.log(error);
            return res.status(500).send(error);
        }
    })();
})

//mencreate data cso
app.post('/cso/create',(req, response)=>{
    (
        async()=>{
            try{
                await db.collection('customerservice').doc()
                .create({
                    nama_lengkap: req.body.nama_lengkap,
                    nama_panggil:req.body.nama_panggil,
                    email: req.body.email
                });
                return response.status(200).send("Data CSO berhasil dibuat");
            }catch(error){
                console.log(error);
                return response.status(500).send(error);
            }
        })();
       // await db.collection('cso_data').doc('/'+req.body.id+'/')
})

//mencreate nomor antrian
app.post('/antrian/create',(req, response)=>{
    (
        async()=>{
            try{
               var now= new Date();
                await db.collection('antrian').doc()
                .create({
                    no_urut : req.body.no_urut,
                    time_ticket : now,
                    status : false
                });
                return response.status(200).send('Nomor Antri berhasil dibuat');
            }catch(error){
                console.log(error);
                return response.status(500).send(error);
            }
        })();
       // await db.collection('cso_data').doc('/'+req.body.id+'/')
})

//mencreate proses panggil antrian
app.post('/panggilantrian/create',(req, response)=>{
    (
        async()=>{
            try{
               var now= new Date();
                await db.collection('panggilantrian').doc()
                .create({
                    id_antrian  : req.body.id_antrian,
                    no_urut     : req.body.no_urut,
                    id_cso      : req.body.id_cso,
                    nama_cso    : req.body.nama_cso,
                    loket       :req.body.loket,
                    time_start  : now,
                    time_finish :now,
                    keterangan   :''
                });
                return response.status(200).send();
            }catch(error){
                console.log(error);
                return response.status(500).send(error);
            }
        })();
       // await db.collection('cso_data').doc('/'+req.body.id+'/')
})

//mengupdate proses panggilan antrian
app.put('/panggilantrian/update/:id',(req,res)=>{
    (async()=> {
        try{
            var now= new Date();
            const document = db.collection('panggilantrian').doc(req.params.id);
            await document.update({
                keterangan : req.body.keterangan,
                time_finish : now
            });
            return res.status(200).send('Data success update');
        }catch(err){
            console.log(err);
            return res.status(500).send(err);
        }
    })();
});

//update tabel antrian
app.put('/antrian/update/:id',(req,res)=>{
    (async()=> {
        try{
            var now= new Date();
            const document = db.collection('antrian').doc(req.params.id);
            await document.update({
                status : req.body.status
            });
            return res.status(200).send('Data success update');
        }catch(err){
            console.log(err);
            return res.status(500).send(err);
        }
    })();
});

exports.app = functions.https.onRequest(app);
